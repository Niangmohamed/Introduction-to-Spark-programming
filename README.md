# Introduction to Spark programming
Part 1: Creating a base RDD and pair RDDs

In this part of the lab, we will explore creating a base RDD with 'parallelize' and using pair RDDs to count words. We'll start by generating a base RDD by using a Python list and the 'sc.parallelize' method. Then we'll print out the type of the base RDD.

Part 2: Apply word count to a file

In this section we will finish developing our word count application. We'll have to build the 'word_count' function, deal with real world problems like capitalization and punctuation, load in our data source, and compute the word count on the new data. First, define a function for word counting. You should reuse the techniques that have been covered in earlier parts of this lab. This function should take in an RDD that is a list of words like `words_RDD` and return a pair RDD that has all of the words and their associated counts.
